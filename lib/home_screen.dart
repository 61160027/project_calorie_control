import 'package:calorie_control/homepage.dart';
import 'package:calorie_control/preson_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  String _email = '';
  void initState() {
    setState(() {
      _email = FirebaseAuth.instance.currentUser!.email!;
    });
    super.initState();
  }

  final auth = FirebaseAuth.instance;

  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: const Color(0xFFE9E9E9),
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            height: height * 0.35,
            left: 0,
            right: 0,
            child: ClipRRect(
              borderRadius: const BorderRadius.vertical(
                bottom: const Radius.circular(30),
              ),
              child: Container(
                color: Colors.white,
                padding: const EdgeInsets.only(
                    top: 50, left: 32, right: 16, bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: [
                        Container(
                          width: 180.0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                  "Hello",
                                  style: TextStyle(fontWeight: FontWeight.w400,
                                  fontSize: 14),
                                ),
                                subtitle: Text(_email,style: TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: 15,
                                  color: Colors.black,

                                ),),
                              ),
                              
                              
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 0, 0, 0),
                          child: RaisedButton(
                            color: Colors.black,
                            child: Text(
                              "ออกจากระบบ",
                              style:
                                  TextStyle(fontSize: 15, color: Colors.white),
                            ),
                            onPressed: () {
                              auth.signOut().then((value) {
                                Navigator.pushReplacement(context,
                                    MaterialPageRoute(builder: (context) {
                                  return Homepage();
                                }));
                              });
                            },
                          ),
                        )
                      ],
                    ),
                    // _RadialProgress{
                    //     width: height* 0.2,
                    //     height: height* 0.2,
                    // },
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            top: height * 0.38,
            left: 0,
            right: 0,
            child: Container(
              height: height,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                      bottom: 8,
                      left: 32,
                      right: 16,
                    ),
                    child: Text(
                      "FOR TODAY",
                      style: const TextStyle(
                          color: Colors.grey,
                          fontSize: 16,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  Expanded(
                      child: SingleChildScrollView(
                    child: Row(
                      children: <Widget>[],
                    ),
                  )),
                  Expanded(
                    child: Container(
                      color: Colors.redAccent,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      color: Colors.blueAccent,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

// class _RadialProgress extends StatelessWidget {
//   final double height, width;

//   const _RadialProgress({Key? key, this.height, this.width}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: height,
//       width: width,
//       color: Colors.grey,
//     );
//   }
// }
