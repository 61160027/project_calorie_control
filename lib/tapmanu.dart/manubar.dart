import 'package:calorie_control/home_screen.dart';
import 'package:calorie_control/preson_screen.dart';
import 'package:calorie_control/sreach_screen.dart';
import 'package:flutter/material.dart';

class ManuBar extends StatefulWidget {
  const ManuBar({ Key? key }) : super(key: key);

  @override
  _ManuBarState createState() => _ManuBarState();
}

class _ManuBarState extends State<ManuBar> {

int currentTabIndex = 0;
  List<Widget> tabs = [
    HomeScreen(),
    SreachScreen(),
    PersonScren()
  ];
  onTapped(int index){
    setState(() {
      currentTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFE9E9E9),
      body: tabs[currentTabIndex],
      bottomNavigationBar: ClipRRect(
        borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
        child: BottomNavigationBar(
          iconSize: 30,
          // selectedIconTheme: IconThemeData(color: const Color(0xFF000000)),
          // unselectedIconTheme: IconThemeData(color: const Color(0x61000000)),
          selectedItemColor: Colors.black,
          onTap: onTapped,
          currentIndex: currentTabIndex,
          items: [
            BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(top: 7.0),
                child: Icon(Icons.home),
              ),
              title: Text(
                "Home",
                style: const TextStyle(color: Colors.white),
              ),
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(top: 7.0),
                child: Icon(Icons.search),
              ),
              title: Text(
                "Search",
                style: const TextStyle(color: Colors.white),
              ),
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(top: 7.0),
                child: Icon(Icons.person),
              ),
              title: Text(
                "Person",
                style: const TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}